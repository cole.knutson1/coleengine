--TODO At least put logic into functions in this file. Eventually refactor
--TODO Figure out how to make loop less redundant
--TODO Make GUID key to all grid items... remove loops altogether (should be doable?)
local image
local bodyToDestroy
local showMenu = false
local dragPlacement = false
local dragStartx;local dragStarty;local dragEndx;local dragEndy;
local activatorSelector = false
local sourceObjectGuid
local singlePlacementLock = false
local selectedLayer = 1



function love.load(arg)
  require("source/startup/gameStart")
  menu:load()
  player:load()
  grid:load()
  love.window.setFullscreen(false)
  stepCount = 0
  selectedItem = nil
end

function love.update(dt)
  if love.keyboard.isDown("f10") then
    selectedItem = nil
  end
  getGridCurrentlyWalkingOn()
  world:update(dt)
  player:update(dt)
  menu:update(dt)
  handleMouse()
  if player.collider:enter('Floorswitch') then
    grid.stepOnActivatableObject()
  end
end

local function drawMain()
  drawDragTiles()
  grid:draw()
  love.graphics.setColor(1, 1, 1, 1)
  if debugMode then
    world:draw()
  end
  player:draw()
  menu:draw()
end

function love.draw()
    drawMain()
end
