-- Link's properties
local playerSpeed = 300
local playerSprintSpeed = 500
walkGrid = anim8.newGrid(400, 600, 1600, 2400)
leftAnimation = anim8.newAnimation(walkGrid('1-4', 3), .1)
rightAnimation = anim8.newAnimation(walkGrid('1-4', 4), .1)
upAnimation = anim8.newAnimation(walkGrid('1-4', 2), .1)
downAnimation = anim8.newAnimation(walkGrid('1-4', 1), .1)
local xColliderOffset, yColliderOffset

player = {}


world:addCollisionClass('Player', {ignores = {'Floorswitch'}, enter = {'Floorswitch)'}})
player.collider = world:newCircleCollider(20, 40, 15)
player.collider:setCollisionClass('Player')
player.anim = downAnimation

function player:update(dt)
  handlePlayerMovement(dt)
end

function player:load()
  xColliderOffset = -30
  yColliderOffset = -70
end

function player:draw()
  local px = player.collider:getX()
  local py = player.collider:getY()
  player.anim:draw(sprites.link, px + xColliderOffset, py + yColliderOffset, 0, .15, .15)
end

function handlePlayerMovement(dt)
  local vectorX = 0
  local vectorY = 0
  player.isMoving = false
  if love.keyboard.isDown("lshift") then
    playerSpeed = playerSprintSpeed
  else
    playerSpeed = 300
  end
  if love.keyboard.isDown("w") then
    vectorY = -1
    player.isMoving = true
    player.direction = "UP"
    player.anim = upAnimation
  end
  if love.keyboard.isDown("a") then
    vectorX = -1
    player.isMoving = true
    player.direction = "LEFT"
    player.anim = leftAnimation
  end
  if love.keyboard.isDown("s") then
    vectorY = 1
    player.isMoving = true
    player.direction = "DOWN"
    player.anim = downAnimation
  end
  if love.keyboard.isDown("d") then
    vectorX = 1
    player.isMoving = true
    player.direction = "RIGHT"
    if(love.mouse.isDown(1)) then
      HandleMouseClick(true)
    end
    player.anim = rightAnimation
  end
  player.collider:setLinearVelocity(vectorX * playerSpeed, vectorY * playerSpeed)

  if player.isMoving then
    playStepSound()
    player.anim:update(dt)
  else
    --disgusting hack
    if player.direction == "RIGHT" then
      player.anim:gotoFrame(2)
    else
      player.anim:gotoFrame(1)
    end
  end
end
