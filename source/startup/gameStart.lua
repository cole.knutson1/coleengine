--First, create physical world
wf = require('libraries/windfield')
world = wf.newWorld(0, 0, true)
world:setGravity(0, 0)
world:addCollisionClass('Floorswitch')
require("source/startup/resources")
require("source/utilityKeypresses")
inspect = require('source/inspect')
handleSound = require('source/handleSound')

anim8 = require('libraries/anim8')
moonshine = require('libraries/moonshine')
grid = require('source/grid')
gridWidth = 50; gridHeight = 50;
gridSize = 40


local player = require('source/player')
debugMode = true
constScreenHeight = love.graphics.getHeight()
constScreenWidth = love.graphics.getWidth()
print("Setup Complete")


function isempty(s)
  return s == nil or s == ''
end


local random = math.random
function newGuid()
    local template ='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    return string.gsub(template, '[xy]', function (c)
        local v = (c == 'x') and random(0, 0xf) or random(8, 0xb)
        return string.format('%x', v)
    end)
end

item = require('source/item')
floorswitch = require('source/floorswitch')
door = require('source/door')
door = require('source/grass')
door = require('source/brick')
menu = require('source/menu')
