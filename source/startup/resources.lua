-- All sprites (images)
sprites = {}
sprites.link = love.graphics.newImage('spritesheets/link.png')
sprites.floorSwitch = love.graphics.newImage("items/floorswitch.png")
sprites.brickWall = love.graphics.newImage("items/brick.png")
sprites.water = love.graphics.newImage("items/water.png")
sprites.grass = love.graphics.newImage("items/grass.png")
sprites.door = love.graphics.newImage("items/door.png")

sprites.items = {}
-- sprites.items.key = love.graphics.newImage('images/key.png')
-- sprites.items.bombs = love.graphics.newImage('images/bombs.png')
-- sprites.items.heartPiece = love.graphics.newImage('images/heartPiece.png')
-- sprites.items.rupees50 = love.graphics.newImage('images/rupees50.png')

-- All fonts
-- fonts = {}
-- fonts.title = love.graphics.newFont("fonts/russoone/RussoOne-Regular.ttf", 42)
-- fonts.debug = love.graphics.newFont("fonts/vt323/VT323-Regular.ttf", 90)
-- fonts.origin = love.graphics.newFont("fonts/vt323/VT323-Regular.ttf", 78)

--All sounds
sounds = {}
sounds.defaultStep = love.audio.newSource( "sounds/step.wav", "static" )
