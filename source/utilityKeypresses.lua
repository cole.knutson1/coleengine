function love.keypressed(key, scancode, isrepeat)
  if key == "escape" then
    love.event.quit(true)
  end
  if key == "f8" then
    debugMode = not debugMode
  end
  if key == "f11" then
    love.window.setFullscreen(not love.window.getFullscreen())
    constScreenHeight = love.graphics.getHeight()
    constScreenWidth = love.graphics.getWidth()
  end
  if key == "return" or key == "f1" then
    menu.toggleMenu()
  end
end
