FloorSwitch = class('FloorSwitch', Item)

-- AgedPerson.static.ADULT_AGE = 18 --this is a class variable
function FloorSwitch:initialize(img)
  Item.initialize(self, img) -- this calls the parent's constructor (Person.initialize) on self
  self.triggerCollider = true
end
