grid = {}
local gridObject = {}

function grid:load()
  createGridObject()
end

function grid:draw()
  local drawPreview = false
  for i = 0, gridWidth do
    for j = 0, gridHeight do
      mx = love.mouse.getX()
      my = love.mouse.getY()
      x = gridObject[i][j].x
      y = gridObject[i][j].y
      love.graphics.setColor(1, 1, 1, .25)

      if mx >= x and mx <= x + gridSize and
      my >= y and my < y + gridSize
      and not isOverMenu then
        drawPreview = true
        gridObject[i][j].isSelected = true
        love.graphics.setColor(0, 1, 0, .75)
      else
        gridObject[i][j].isSelected = false
      end
      if(gridObject[i][j].isPopulated) then
        love.graphics.setColor(1, 1, 1, 1)
        if(gridObject[i][j]["activated"]) then
          love.graphics.setColor(0, 1, 0, 1)
        end

        love.graphics.draw(gridObject[i][j].img, gridObject[i][j].x, gridObject[i][j].y)
        else if gridObject[i][j].isBeingWalkedOn and debugMode then
          love.graphics.setColor(0, 0, 1, 1)
          love.graphics.rectangle("line", gridObject[i][j].x, gridObject[i][j].y, gridSize, gridSize)
        else
          if (debugMode) then
            love.graphics.setColor(1, 1, 1, .25)
            love.graphics.rectangle("line", gridObject[i][j].x, gridObject[i][j].y, gridSize, gridSize)
          end
        end

      end
      if drawPreview and gridObject[i][j].isSelected then
        drawItem(x, y)
      end
    end
  end

end

function createGridObject()
  for i = 0, gridWidth do
    gridObject[i] = {} -- create a new row
    local g = newGuid()
    for j = 0, gridHeight do

      gridObject[i][j] = {x = i * gridSize, y = j * gridSize, isSelected = false, populatingItem = nil, newWall = nil, guid = g}
      -- gridObject[i][j] = { isSelected = false, isPopulated = nil, colliderBox = nil, guid = g}
    end
  end
end

function getGridCurrentlyWalkingOn()
  for i = 0, gridWidth do
    for j = 0, gridHeight do
      mx = player.collider:getX()
      my = player.collider:getY()
      x = gridObject[i][j].x
      y = gridObject[i][j].y
      love.graphics.setColor(1, 1, 1, .25)
      -- if mx >= x and mx <= x + gridSize and
      -- my >= y and my < y + gridSize then
      --   gridObject[i][j].isBeingWalkedOn = true
      -- else
      --   gridObject[i][j].isBeingWalkedOn = false
      -- end
    end
  end
end


function handleMouse()
  if(love.mouse.isDown(1)) then
    HandleMouseClick(true)
  end
  if(love.mouse.isDown(2)) then
    HandleMouseClick(false)
  end
end

function HandleMouseClick(doCreate)
  if dragPlacement then
    for i = 0, gridWidth do
      for j = 0, gridHeight do
        mx = love.mouse.getX()
        my = love.mouse.getY()
        x = gridObject[i][j].x
        y = gridObject[i][j].y
        if mx >= x and mx <= x + gridSize and
        my >= y and my < y + gridSize then
          dragEndx = i; dragEndy = j;
        end
      end
    end
    return
  end

  --Handle click of non-menu
  if not menu.isMouseOverMenu and not singlePlacementLock then
    for i = 0, gridWidth do
      for j = 0, gridHeight do

        --Potentially populate the square with selected item
        if gridObject[i][j].isSelected and not isempty(selectedItem) then
          if not love.keyboard.isDown("lshift") then

            --Destroy whatever exists at this spot
            colliders_2 = world:queryLine(gridObject[i][j].x - 1, gridObject[i][j].y + 1, gridObject[i][j].x + 10, gridObject[i][j].y + 1, {'All', except = {'Player'}})
            gridObject[i][j].isPopulated = nil
            for _, collider in ipairs(colliders_2) do
              collider:destroy()
            end

            --Create new object
            if doCreate then
              -- print("hackyHack =  "..selectedItem.class.name..":new(selectedItem.img,false,"..i * gridSize..","..j * gridSize..","..gridSize..")")
              singlePlacementLock = true

              --Create collision
              if selectedItem["isCollidable"] or selectedItem["triggerCollider"] then
                gridObject[i][j].colliderBox = world:newRectangleCollider(gridObject[i][j].x, gridObject[i][j].y, gridSize, gridSize)
                gridObject[i][j].colliderBox:setType('static')
                --Create trigger collision
                if selectedItem["triggerCollider"] then
                  gridObject[i][j].colliderBox:setCollisionClass('Floorswitch')
                  sourceObjectGuid = gridObject[i][j].guid
                  sourceObjectI = i
                  sourceObjectJ = j
                  selectedItem = nil
                  activatorSelector = true
                end
              end
            end

            --Drag population
            else if love.keyboard.isDown("lshift") and doCreate then
              dragStartx = i; dragStarty = j;
              dragPlacement = true
              return
            end
          end

          --Try to select trigger object for source trigger
          else if activatorSelector then
            if gridObject[i][j].isSelected and not isempty(gridObject[i][j].isPopulated) and (i ~= sourceObjectI or j ~= sourceObjectJ) then
              activatorSelector = false
              gridObject[sourceObjectI][sourceObjectJ].triggerGuid = gridObject[i][j].guid
              print("SELECTED: "..gridObject[sourceObjectI][sourceObjectJ].triggerGuid )
              singlePlacementLock = true
            end
          end
        end

        --Selector mode (move to above block)
        if gridObject[i][j].isSelected and isempty(selectedItem) and not singlePlacementLock then
          sourceObjectGuid = gridObject[i][j].guid
          activatorSelector = true
        end
      end
    end
    else if not singlePlacementLock then
      selectedItem = menu:handleClick()
      print(selectedItem)
      activatorSelector = false
    end
  end
end

function love.mousereleased(x, y, button, isTouch)
  singlePlacementLock = false
  if dragPlacement then
    dragPlacement = false
    placeDragTiles()
  end
end
--
-- function placeDragTiles()
--   if dragStartx > dragEndx then
--     local tempStartx = dragStartx
--     dragStartx = dragEndx
--     dragEndx = tempStartx
--   end
--   if dragStarty > dragEndy then
--     local tempStarty = dragStarty
--     dragStarty = dragEndy
--     dragEndy = tempStarty
--   end
--   for i = 0, gridWidth do
--     for j = 0, gridHeight do
--
--       if i >= dragStartx and i <= dragEndx and
--       j >= dragStarty and j <= dragEndy then
--         colliders_2 = world:queryLine(gridObject[i][j].x - 1, gridObject[i][j].y + 1, gridObject[i][j].x + 10, gridObject[i][j].y + 1, {'All', except = {'Player'}})
--         for _, collider in ipairs(colliders_2) do
--           collider:destroy()
--         end
--         if selectedItem.class.name ~= "FloorSwitch" then
--           gridObject[i][j] = Item:new(selectedItem.img, false,i * gridSize, j * gridSize, gridSize)
--         else
--           print("HI")
--           -- gridObject[i][j] = Item:new(selectedItem.img, false,i * gridSize, j * gridSize, gridSize)
--         end
--
--         -- gridObject[i][j].activated = false
--         -- gridObject[i][j].isPopulated = selectedItem
--         -- print(inspect(selectedItem))
--         -- if selectedItem["isCollidable"] or selectedItem["triggerCollider"] then
--         --   gridObject[i][j].colliderBox = world:newRectangleCollider(gridObject[i][j].x, gridObject[i][j].y, gridSize, gridSize)
--         --   gridObject[i][j].colliderBox:setType('static')
--         --   if selectedItem["triggerCollider"] then
--         --     gridObject[i][j].colliderBox:setCollisionClass('Floorswitch')
--         --   end
--         -- end
--       end
--     end
--   end
--   dragStartx = 0;dragStarty = 0;dragEndx = 0;dragEndy = 0;
-- end

function drawDragTiles()
  local dummyStartx = dragStartx
  local dummyStarty = dragStarty
  local dummyEndx = dragEndx
  local dummyEndy = dragEndy
  if isempty(dummyStartx) or isempty(dummyStarty) or isempty(dummyEndx) or isempty(dummyEndy)
  or (dragStartx == 0 and dragStarty == 0) then
    return
  end

  if dummyStartx > dummyEndx then
    local tempStartx = dummyStartx
    dummyStartx = dummyEndx
    dummyEndx = tempStartx
  end
  if dummyStarty > dummyEndy then
    local tempStarty = dummyStarty
    dummyStarty = dummyEndy
    dummyEndy = tempStarty
  end


  for i = 0, gridWidth do
    for j = 0, gridHeight do

      if i >= dummyStartx and i <= dummyEndx and
      j >= dummyStarty and j <= dummyEndy then
        love.graphics.draw(selectedItem["img"], gridObject[i][j].x, gridObject[i][j].y)
      end
    end
  end
end

function activateTriggerOfSelectedGridObject(guidToDelete)
  for i = 0, gridWidth do
    for j = 0, gridHeight do
      if gridObject[i][j].guid == guidToDelete then
        gridObject[i][j]:onActivated()
      end
    end
  end
end

grid.stepOnActivatableObject = function()
  local found = false
  local collision_data = player.collider:getEnterCollisionData('Floorswitch')
  for i = 0, gridWidth do
    for j = 0, gridHeight do
      if not isempty(gridObject[i][j].colliderBox) and gridObject[i][j].colliderBox["id"] == collision_data["collider"]["id"] then--and gridObject[i][j].colliderBox["id"] == collision_data["id"] then
        gridObject[i][j].activated = true
        activateTriggerOfSelectedGridObject(gridObject[i][j].triggerGuid)
        found = true
        break
      end
      if found then
        break
      end
    end
  end
end

function drawItem(x, y)
  if not isempty(selectedItem) then
    love.graphics.setColor(1, 1, 1, .5)
    love.graphics.draw(selectedItem["img"], x, y)
  end
end

return grid
