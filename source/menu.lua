menu = {}
local showMenu = true
local menuY = 10
local menuMargin = 10
local menuPadding = 15
local height = 100

menu.itemArray = {
  --For all non-generic items, add sprite name through class after init
  floorswitch = {img = love.graphics.newImage("items/floorswitch.png"), x = nil, y = nil, x2 = nil,
                    y2 = nil, isSelected = false, isCollidable=false, triggerCollider=true,
                  activated = false},
    brick = {img = love.graphics.newImage("items/brick.png"), x = nil, y = nil, x2 = nil, y2 = nil, isSelected = false, isCollidable=true},
    water = {img = love.graphics.newImage("items/water.png"), x = nil, y = nil, x2 = nil, y2 = nil, isSelected = false, isCollidable=false},
    grass = {img = love.graphics.newImage("items/grass.png"), x = nil, y = nil, x2 = nil, y2 = nil, isSelected = false, isCollidable=false},
    door = {img = love.graphics.newImage("items/door.png"), x = nil, y = nil, x2 = nil, y2 = nil, isSelected = false, isCollidable=true}
  }

-- table.insert(menu.itemArray,Door:new(door,true))
menu.isMouseOverMenu = false

function menu:load()
  iItemArray = -1
  for k, v in pairs(menu.itemArray) do
    iItemArray = iItemArray + 1
    v["x"] = 50 * iItemArray + 15
    v["y"] = 20
    v["x2"] = v["x"]+40
    v["y2"] = v["y"]+40
  end
end

function menu:draw()
  if showMenu then
    if menu.isMouseOverMenu then
      love.graphics.setColor(.5, .5, 0, 1)
    else
      love.graphics.setColor(.5, .5, 0, .7)
    end
    love.graphics.rectangle('fill', 0, 0, constScreenWidth, height)
    love.graphics.setColor(1, 1, 1, 1)
    local iMenu = 0
    for _, v in pairs(menu.itemArray) do
      love.graphics.setColor(1, 1, 1, 1)
      -- if(v["isSelected"]) then
      --   love.graphics.setColor(0, .6, 0, 1)
      -- end
      love.graphics.draw(v.img, v.x, v.y)
    end
  end
end

function menu:update(dt)
  checkIsMouseOverMenu()
  checkIsMouseOverItem()
end

function checkIsMouseOverMenu()
  my = love.mouse.getY()
  menu.isMouseOverMenu = false
  if my < height and showMenu then
    menu.isMouseOverMenu = true
  end
end

function checkIsMouseOverItem()
  mx = love.mouse.getX()
  my = love.mouse.getY()
  for k, v in pairs(menu.itemArray) do
    if mx >= v["x"] and mx <= v["x2"] and
    my >= v["y"] and my < v["y2"] then
      v["isSelected"] = true
    else
      v["isSelected"] = false
    end
  end
end

menu.handleClick = function()
  mx = love.mouse.getX()
  my = love.mouse.getY()
  for k, v in pairs(menu.itemArray) do
    if v["isSelected"] then
      return v
    end
  end
end

menu.toggleMenu = function()
  showMenu = not showMenu
end
return menu
