function playStepSound()
  love.audio.play(sounds.defaultStep)
  stepCount = stepCount+1
  if(stepCount > 12) then
    love.audio.stop(sounds.defaultStep)
    stepCount = 0
  end
end
